﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Daba hna ghandiro wa7d l3iba
// m3a ghadi n7alo Fentre andiro Connecter()
// Button Fermer ola ila 7bassat app ndir Deconnecter()
// Hadhchi howa li kayn f Form1_Load() & Form1_Close()


namespace _2A_201_CTP1
{
    public partial class Form1 : Form
    {

        // Bach nstam3lo dakchi dyal SQL rah deja sawbnah f wa7d class
        // dik class saminaha Database
        // 3ayatna 3liha bach nsta3mloha
        private Database db;

        public Form1()
        {
            InitializeComponent();
        }


        // Mnin yallah ghat7al la fenetre ghadi i3ayat 3la had methode
        // Had methode dyal Fenetre 
        // Had methode 3ndha 3ala9a b Form1
        // ila bghiti t3rf ktar sir l Form1.Designer.cs ghatal9a bli
        // Rah matankdabch 3likom
        // .....
        // rah hwa this.Load += new System.EventHandler(this.Form1_Load);
        // .
        private void Form1_Load(object sender, EventArgs e)
        {
            // Awal 7aja tadiro initialisation dyal Objet dyalna 
            db = new Database();

            // 7na bghina m3a t7al fentre ndiro Connecter
            // dakchi 3lach 7atinaha wast Form1_Load
            db.Connecter();

            // Dir refresh l dataGrid
            RefreshDataGrid();
        }


        // Hdi b7al dyal Form1_Load ghir dyal SADDAN
        // sir rja3 o9alab 3la 
        // this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
        // Bla matsawlani fin ghatmchi
        // Ga3 les declaration ghatl9ahom f Form1.Designer.cs 
        // bdapt f InitializeComponent
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Deconnecter();
        }



        /// <summary>
        /// 
        /// HADCHI KAML LI LTA7T RAH GHIR CLICK
        /// 
        /// bghiti t3rf ktar rak 3arf fin tmchi
        /// 
        /// 
        /// </summary>





        // Maghadich 3aaaaaaaaaad ndiro connecter
        // tanftardo bli 7na deja m connecteyin (Form1_Load m3a t7alat fenetre dart connecter)
        // connection mazala OPEN
        // Donc diro gha dakchi li mohim
        // Command .....
        private void btnAjouter_Click(object sender, EventArgs e)
        {

            // Hadi 7fdoooooooooooha
            // HYa bach tansawbo les commandes
            // Liaison bin language dyal C# & SQL Server
            db.cmd.CommandText = "INSERT INTO Etudiant values(@cin, @nom, @prenom, @age)";

            // Kifach ghadi n3rf fin ghadi n executer had commande ??????
            // touuuuut simplement
            // ghadi tgolih had commande dirha f connection li an3tik
            // db.cmd.Connection: Jay mn 3ndk commande li jaya mn 3nd classe database li sawbna (Line 11 f Database.cs)
            // db.connection: Jay mn 3nd classe database li sawbna (Line 10 f Database.cs)
            db.cmd.Connection = db.connection;

            // Daba sawbti cnx osawbi cmd ??
            // Oui
            // iwa dok CIN NOM fin homa les valeurs dyalhom????
            // ghan7atohom les Parametres dyal Commande (CMD)
            // had les parametres homa CIN, NOm, ....
            // les parametres khass it7ato f commande machi f connection ola chi 9ant akhor

            // Tafadyan li ay tari2 kharij 3an iradatina
            // supprimiw zmar kaml bach mayb9ach 3a9l 3la l9dim
            db.cmd.Parameters.Clear();

            // Onbdaw saf7a jdida
            // sir 3awdw bda tfakar 1000 mrrat bach tgolia fin andiroha
            // Paramtre > 3ndo 3la9a b commande

            // sammmi dakchi ki samitih f star lfo9ani
            // 9bal matbda 7adi m3a USER rah mafih ti9a
            // 9alab wach les valeurs 3amrin ola lla 
            // la ti9a f 3ati9a

            // Enum 7it la valeur par def dyalhom hya 0 dakchi 3lach tan testew b 0
            // ammma age gha bghit 
            if (numID.Value != 0 && txtNom.Text != "" && txtPrenom.Text != "" && numAge.Value > 18) {
                db.cmd.Parameters.AddWithValue("@cin", numID.Value);
                db.cmd.Parameters.AddWithValue("@nom", txtNom.Text);
                db.cmd.Parameters.AddWithValue("@prenom", txtPrenom.Text);
                db.cmd.Parameters.AddWithValue("@age", numAge.Value);

                // Arak lfraja
                // executer dakachi 
                db.cmd.ExecuteNonQuery();

                // Mora insertion andir refresh
                RefreshDataGrid();
            }
            else
            {
                MessageBox.Show("Merci de remplir tous les champs");
            }



        }

        private void btnModifier_Click(object sender, EventArgs e)
        {

        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {

        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            db.Deconnecter();
            this.Close();
        }



        // HADI METHODE ANA LI MSSAWBHA
        // gharad mnha hya dir Refresh
        public void RefreshDataGrid()
        {

            // Recuperet Table Etudiant mn la base dyali
            // gha hwa Jat b7al haka
            // SQL_CLIENT(table_etudiant) -> li hwaa SqlDataReader
            db.dataReader = db.GetAll();

            // Daba khssni n transformer hadkchi dyal SqlDataReader
            // vers Table tay3rfa DataGridView

            // Creer wa7d DataTable khawi
            DataTable dataTable = new DataTable();

            // Ghadi n3amro b DataReader dyali li jabt mn star lfo9ani (GetAll)
            dataTable.Load(db.dataReader);

            // Daba 3aaaaad ghadi na3ti l DataGridView DataTable
            // hadchi li tat3rf hya
            dataGridView.DataSource = dataTable;
        }

    }



}
