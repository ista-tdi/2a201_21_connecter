﻿using System.Data.SqlClient;
using System.Data;

namespace _2A_201_CTP1
{

    class Database
    {

        public SqlConnection connection = new SqlConnection();
        public SqlCommand cmd = new SqlCommand();


        // Read
        // rani madartch initialisation
        // walakin li ghay3mro hwa select li ghada tjih
        // chofo RefreshDataGrid rah hya li mkhadma had attribute
        // kat3amro b resultat dyal ExecuteReader()
        // li hya la table dyali sous form dyal Objet Sql (Provider li mn sba7 wana nhdr 3lih)
        public SqlDataReader dataReader;
        

        public void Connecter()
        {
            connection.ConnectionString = "Data Source=DESKTOP-HFLAVK5;Initial Catalog=TP1;Integrated Security=True";
            
            if(connection.State.Equals(ConnectionState.Closed) ||
                connection.State.Equals(ConnectionState.Broken))
            {
                try { 
                    connection.Open();
                }
                catch
                {
                    System.Console.WriteLine("Failed to connect");
                }
            }
        }



        public void Deconnecter()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                try
                {
                    connection.Close();
                }
                catch
                {
                    System.Console.WriteLine("Failed to close DB connection");
                }
            }
        }




        // Hadi ghada t recuperer lia la table Etudiant kaaaaaaaamla
        public SqlDataReader GetAll()
        {
            cmd.CommandText = "SELECT * FROM Etudiant";
            // executer la commande b connection li deja 3ndi 
            // had lblan tandiro bach nsta3mal connection li deja open odej aana khdam biha
            cmd.Connection = connection;
            return cmd.ExecuteReader();
        }


    }
}
